﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StoryPathManager.Entities.Faction;
using StoryPathManager.Entities.Faction.Create;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests.TestData;

namespace Tests.Entities
{
    [TestClass]
    public class TestFactionCreation
    {

        [TestMethod]
        public void Test_ParseSimplestFactionPathComboIntoFactions()
        {
            // Given input
            string factionsTextBlock = FactionsData.twoFactionsWith9And6Paths_OList;

            // When
            List<StoryFaction> factions = new CreateFaction().MultipleFactions(factionsTextBlock);

            // Then
            Assert.IsTrue(factions.Count == 2, "The parser did not detect 2 factions but: " + factions.Count);
            Assert.IsTrue(factions[0].StoryPaths.Count == 9, "Factions[0] does not have 9 story paths. Has: " +
                factions[0].StoryPaths.Count);
            Assert.IsTrue(factions[1].StoryPaths.Count == 6, "Factions[1] does not have 6 story paths. Has: " +
                factions[1].StoryPaths.Count);

            // Given expectations
            string expectedDisplay = "Infestacja Cywili: 0/8";

            // When
            string display = factions[0].StoryPaths[0].ToDisplay();

            // Then result
            Assert.IsTrue(display == expectedDisplay, "Expected display does not match actual display");
        }

    }

}

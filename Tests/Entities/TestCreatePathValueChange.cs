﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StoryPathManager.Entities.ChangeFactionPath;
using StoryPathManager.Entities.ChangeFactionPath.Create;
using StoryPathManager.Technical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Entities
{
    [TestClass]
    public class TestCreatePathValueChange
    {
        [TestMethod]
        public void Test_CreatePVC_WithParameters_NoSpaces()
        {
            // Given
            string inputData = "111,22,33";

            // Expecting
            PathValueChange expected = new PathValueChange(111, 22, 33);

            // When
            PathValueChange actual = new CreatePathValueChange().FromText(inputData);

            // Then
            Assert.IsTrue(expected.AllFields().Equals(actual.AllFields()));

        }

        [TestMethod]
        public void Test_CreatePVC_WithParameters_WithSpaces()
        {
            // Given
            string inputData = " 111,   22 , 33  ";

            // Expecting
            PathValueChange expected = new PathValueChange(111, 22, 33);

            // When
            PathValueChange actual = new CreatePathValueChange().FromText(inputData);

            // Then
            Assert.IsTrue(expected.AllFields().Equals(actual.AllFields()));

        }

        [TestMethod]
        public void Test_CreatePVC_WithParameters_BeingWrongSoNullObject()
        {
            // Given
            string inputData = "kladsaldjsaldsm";

            // Expecting
            PathValueChange expected = new PathValueChange(0, 0, 0);

            // When
            PathValueChange actual = new CreatePathValueChange().FromText(inputData);

            // Then
            Assert.IsTrue(expected.AllFields().Equals(actual.AllFields()));

        }

    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StoryPathManager.Entities.AddFactionPath;
using StoryPathManager.Entities.AddFactionPath.Create;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Entities
{
    [TestClass]
    public class TestCreatePathAdditionChange
    {
        [TestMethod]
        public void Test_CreatePACWithParameters_GoodSpaces()
        {
            // Given
            string inputData = "3, 0, 10, Admiral Bubuta rules unopposed";

            // Expecting
            PathAdditionChange expected = new PathAdditionChange(3, 0, 10, "Admiral Bubuta rules unopposed");

            // When
            PathAdditionChange actual = new CreatePathAdditionChange().FromText(inputData);

            // Then
            Assert.IsTrue(expected.AllFields().Equals(actual.AllFields()));
        }

        [TestMethod]
        public void Test_CreatePACWithParameters_StrangeSpaces()
        {
            // Given
            string inputData = "   3,0,        10   , Admiral Bubuta rules unopposed   ";

            // Expecting
            PathAdditionChange expected = new PathAdditionChange(3, 0, 10, "Admiral Bubuta rules unopposed");

            // When
            PathAdditionChange actual = new CreatePathAdditionChange().FromText(inputData);

            // Then
            Assert.IsTrue(expected.AllFields().Equals(actual.AllFields()));
        }

        [TestMethod]
        public void Test_CreatePACWithParameters_BeingWrong()
        {
            // Given
            string inputData1 = "f, 13";
            string inputData2 = "8 13";

            // Expecting
            PathAdditionChange expected = new PathAdditionChange(0, 0, 0, string.Empty);

            // When
            PathAdditionChange actual1 = new CreatePathAdditionChange().FromText(inputData1);
            PathAdditionChange actual2 = new CreatePathAdditionChange().FromText(inputData2);

            // Then
            Assert.IsTrue(expected.AllFields().Equals(actual1.AllFields()));
            Assert.IsTrue(expected.AllFields().Equals(actual2.AllFields()));
        }
    }
}

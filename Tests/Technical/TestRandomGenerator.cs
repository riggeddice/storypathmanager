﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StoryPathManager.Technical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.Technical
{
    [TestClass]
    public class TestRandomGenerator
    {

        [TestMethod]
        public void Test_RandomGenerator_Between1And1()
        {
            // Given
            int minRange = 1;
            int maxRange = 1;

            // When
            int result = new RandomGenerator().GenerateBetween(minRange, maxRange);

            // Then
            Assert.IsTrue(result == 1);
        }

        [TestMethod]
        public void Test_RandomGenerator_Between5And5()
        {
            // Given
            int minRange = 5;
            int maxRange = 5;

            // When
            int result = new RandomGenerator().GenerateBetween(minRange, maxRange);

            // Then
            Assert.IsTrue(result == 5);
        }

        [TestMethod]
        public void Test_RandomGenerator_FromRange2And4()
        {
            // Given
            List<int> range = new List<int>();
            range.Add(2);
            range.Add(3);
            range.Add(4);

            // When
            bool inRange = true;
            for (int i=0; i<100; i++)
            {
                int result = new RandomGenerator().GenerateFromRange(range);
                if (result != 2 && result !=3 && result != 4)
                {
                    inRange = false;
                }
            }
            
            // Then
            Assert.IsTrue(inRange);
        }

        [TestMethod]
        public void Test_RandomGenerator_FromRange2And4Without3()
        {
            // Given
            List<int> range = new List<int>();
            range.Add(2);
            range.Add(4);

            // When
            bool inRange = true;
            for (int i = 0; i < 100; i++)
            {
                int result = new RandomGenerator().GenerateFromRange(range);
                if (result != 2 && result != 4)
                {
                    inRange = false;
                }
            }

            // Then
            Assert.IsTrue(inRange);
        }

        [TestMethod]
        public void Test_RandomGenerator_FromRange7()
        {
            // Given
            List<int> range = new List<int>();
            range.Add(7);

            // When
            bool inRange = true;
            for (int i = 0; i < 100; i++)
            {
                int result = new RandomGenerator().GenerateFromRange(range);
                if (result != 7)
                {
                    inRange = false;
                }
            }

            // Then
            Assert.IsTrue(inRange);
        }
    }
}

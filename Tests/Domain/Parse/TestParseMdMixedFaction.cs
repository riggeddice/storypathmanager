﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StoryPathManager.Domain.ParseText.Faction;
using System;
using System.Collections.Generic;
using Tests.TestData;

namespace Tests.Domain.Parse
{
    [TestClass]
    public class TestParseMdMixedFaction
    {

        [TestMethod]
        public void Test_FactionTestBlockIntoFactionTexts()
        {
            // Given
            string givenText = FactionsData.twoFactionsWith9And6Paths_Mixed;

            // When
            List<string> splitText = new ParseMdMixedFaction().SplitIntoSingleFactionTextBlocks(givenText);

            // Then
            string recombinedText = String.Join(string.Empty, splitText.ToArray());
            Assert.IsTrue(string.Equals(givenText, recombinedText));
        }

    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StoryPathManager.Domain.FormatText;
using StoryPathManager.Entities.Faction;
using StoryPathManager.Entities.Faction.Create;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests.TestData;

namespace Tests.Domain
{
    [TestClass]
    public class TestFormatStateToDisplay
    {
        [TestMethod]
        public void Test_FormatState_DisplayProperlyFormats()
        {
            // Given data and expectations
            string inputText = FactionsData.twoFactionsWith9And6Paths_OList;

            string expected_1_FactionFormatting = "1. Bzizma";
            string expected_2_FactionFormatting = "2. Mausożercy";
            string expectedAnyPathFormatting = "    5. Podpięcie się do leylinów: 6/8";

            // Given pre-actions
            List<StoryFaction> factions = new CreateFaction().MultipleFactions(inputText);

            // When
            string formattedOutput = new FormatInternalState().Factions(factions);

            // Then
            Assert.IsTrue(formattedOutput.Contains(expected_1_FactionFormatting));
            Assert.IsTrue(formattedOutput.Contains(expected_2_FactionFormatting));
            Assert.IsTrue(formattedOutput.Contains(expectedAnyPathFormatting));
        }

        [TestMethod]
        public void Test_FormatState_StoreProperlyFormats()
        {
            // Given data and expectations
            string inputText = FactionsData.twoFactionsWith9And6Paths_OList;

            string expected_1_FactionFormatting = "1. Bzizma:";
            string expected_2_FactionFormatting = "2. Mausożercy";
            string expectedAnyPathFormatting = "    5. Podpięcie się do leylinów: 6/8";

            // Given pre-actions
            List<StoryFaction> factions = new CreateFaction().MultipleFactions(inputText);

            // When
            string formattedOutput = new FormatInternalState().Factions(factions);

            // Then
            Assert.IsTrue(formattedOutput.Contains(expected_1_FactionFormatting));
            Assert.IsTrue(formattedOutput.Contains(expected_2_FactionFormatting));
            Assert.IsTrue(formattedOutput.Contains(expectedAnyPathFormatting));
            Assert.IsTrue(formattedOutput.Contains("\n\n") == false);
        }
    }
}

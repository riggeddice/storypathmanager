﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StoryPathManager.Domain.StoryFactionsPaths;
using StoryPathManager.Entities.AddFactionPath;
using StoryPathManager.Entities.AddFactionPath.Create;
using StoryPathManager.Entities.Faction;
using StoryPathManager.Entities.Faction.Create;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests.TestData;

namespace Tests.Domain
{
    [TestClass]
    public class TestAddPathToSpecificFaction
    {
        [TestMethod]
        public void Test_AddPathToFaction_ProperlyAddsFor_F1P1I3()
        {
            // Given raw data
            string factionsText = FactionsData.twoFactionsWith9And6Paths_OList;
            string newfactionName = "Admiral Bubuta";
            string addedPathChangeText = "1, 0, 10, " + newfactionName;

            // Given actual input
            List<StoryFaction> factions = new CreateFaction().MultipleFactions(factionsText);
            PathAdditionChange change = new CreatePathAdditionChange().FromText(addedPathChangeText);

            // Expecting
            int previousValue = factions[0].StoryPaths.Count;
            int expectedChange = 1;

            // When
            factions = new AddNewStoryPathToSpecificFaction().Do(factions, change);

            // Then
            int actualValue = factions[0].StoryPaths.Count;
            Assert.IsTrue(previousValue + expectedChange == actualValue);
            Assert.IsTrue(factions[0].StoryPaths[actualValue - 1].PathName == newfactionName);
        }
    }
}

﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using StoryPathManager.Domain.StoryActions;
using StoryPathManager.Domain.StoryActions.Create;
using StoryPathManager.Entities.Faction;
using StoryPathManager.Entities.Faction.Create;
using StoryPathManager.Technical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests.TestData;

namespace Tests.Domain
{
    [TestClass]
    public class TestAddSingleStoryAction
    {
        [TestMethod]
        public void Test_AddSingleAction_ProperlyAddsForBothFactions()
        {
            // Given data and expectations
            string inputText = FactionsData.twoFactionsWith9And6Paths_OList;

            List<int> firstFactionOptions = new List<int>();
            firstFactionOptions.AddRange(Enumerable.Range(0, 9).ToArray());

            List<int> secondFactionOptions = new List<int>();
            secondFactionOptions.AddRange(Enumerable.Range(0, 6).ToArray());

            // Given mocks
            var ranGen = new Mock<RandomGenerator>();

            //      First faction will increment 3rd path (index 2)
            ranGen.Setup(f => f.GenerateFromRange(firstFactionOptions)).Returns(2);

            //      Second faction will increment 1st path (index 0)
            ranGen.Setup(f => f.GenerateFromRange(secondFactionOptions)).Returns(0);

            // Given pre-actions
            List<StoryFaction> factions = new CreateFaction().MultipleFactions(inputText);
            var addStoryAction = new CreateAddSingleStoryAction().WithCustomRandomGenerator(ranGen.Object);

            // Assuming previous state
            Assert.IsTrue(factions[0].StoryPaths[2].CurrentValue == 2);
            Assert.IsTrue(factions[1].StoryPaths[0].CurrentValue == 0);

            // When
            List<StoryFaction> updatedfactions = addStoryAction.IncrementRandomStoryPathInFactions(factions);

            // Then
            Assert.IsTrue(updatedfactions[0].StoryPaths[2].CurrentValue == 3);
            Assert.IsTrue(updatedfactions[1].StoryPaths[0].CurrentValue == 1);

            for (int i = 1; i < updatedfactions[1].StoryPaths.Count; i++)
            {
                // Nothing else changed
                Assert.IsTrue(updatedfactions[1].StoryPaths[i].CurrentValue == factions[1].StoryPaths[i].CurrentValue);
            }

        }


        [TestMethod]
        public void Test_AddSingleAction_AddsOnlyWhenValid()
        {
            // Given data and expectations
            string inputText = FactionsData.oneFaction4Paths3Completed20ActionsToGo_OList;

            // Given pre-actions
            List<StoryFaction> factions = new CreateFaction().MultipleFactions(inputText);
            var addStoryAction = new CreateAddSingleStoryAction().WithCustomRandomGenerator(new RandomGenerator());

            for (int i = 0; i < 20; i++)
            {
                // When
                // 20 times in a row it should generate the ONLY possible path
                factions = addStoryAction.IncrementRandomStoryPathInFactions(factions);
                Assert.IsTrue(factions[0].StoryPaths[1].CurrentValue == i + 1);
            }

        }
    }
}

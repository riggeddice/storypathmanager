﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using StoryPathManager.Domain.StoryActions.AddMultiple;
using StoryPathManager.Entities.ChangeFactionPath;
using StoryPathManager.Entities.ChangeFactionPath.Create;
using StoryPathManager.Entities.Faction;
using StoryPathManager.Entities.Faction.Create;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Tests.TestData;

namespace Tests.Domain
{
    [TestClass]
    public class TestAddXStoryActionsToSpecificPath
    {

        [TestMethod]
        public void Test_AddXActions_ProperlyAddsFor_F1P1I3()
        {
            // Given raw data
            string factionsText = FactionsData.twoFactionsWith9And6Paths_OList;
            string factionPathChangeText = "1,1,3";

            // Given actual input
            List<StoryFaction> factions = new CreateFaction().MultipleFactions(factionsText);
            PathValueChange change = new CreatePathValueChange().FromText(factionPathChangeText);

            // Expecting
            int previousValue = factions[0].StoryPaths[0].CurrentValue;
            int expectedChange = 3;

            // When
            factions = new AddXStoryActionsToSpecificPath().Do(factions, change);

            // Then
            int actualValue = factions[0].StoryPaths[0].CurrentValue;
            Assert.IsTrue(previousValue + expectedChange == actualValue);
        }

        [TestMethod]
        public void Test_AddXActions_ProperlyAdds_ExceedingMaxValue()
        {
            // Given raw data
            string factionsText = FactionsData.twoFactionsWith9And6Paths_OList;
            string factionPathChangeText = "1,1,9999";

            // Given actual input
            List<StoryFaction> factions = new CreateFaction().MultipleFactions(factionsText);
            PathValueChange change = new CreatePathValueChange().FromText(factionPathChangeText);

            // Expecting
            int previousValue = factions[0].StoryPaths[0].CurrentValue;
            int expectedResult = factions[0].StoryPaths[0].MaxValue;

            // When
            factions = new AddXStoryActionsToSpecificPath().Do(factions, change);

            // Then
            int actualValue = factions[0].StoryPaths[0].CurrentValue;
            Assert.IsTrue(expectedResult == actualValue);
            Assert.IsTrue(actualValue != previousValue);
        }

        [TestMethod]
        public void Test_AddXActions_ProperlySubtracts_BelowMinValue()
        {
            // Given raw data
            string factionsText = FactionsData.twoFactionsWith9And6Paths_OList;
            string factionPathChangeText = "1,2,9999";

            // Given actual input
            List<StoryFaction> factions = new CreateFaction().MultipleFactions(factionsText);
            PathValueChange change = new CreatePathValueChange().FromText(factionPathChangeText);

            // Expecting
            int previousValue = factions[0].StoryPaths[1].CurrentValue;
            int expectedResult = 0;

            // When
            factions = new RemoveXStoryActionsFromSpecificPath().Do(factions, change);

            // Then
            int actualValue = factions[0].StoryPaths[1].CurrentValue;
            Assert.IsTrue(expectedResult == actualValue);
            Assert.IsTrue(actualValue != previousValue);
        }
    }
}

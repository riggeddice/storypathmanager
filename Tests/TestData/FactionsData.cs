﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests.TestData
{
    public class FactionsData
    {
        public static readonly string twoFactionsWith9And6Paths_Mixed = @"**Bzizma**:

* Infestacja Cywili:                                                    0/8
* Infestacja Żywności i Ekonomii:                                       3/6     (1 tier)
* Regeneracja i Adaptacja Bzizmy:                                       2/n     (1 tier)
* Produkcja Rojów:                                                      2/8     (1 tier)
* Podpięcie się do leylinów:                                            6/8     (3 tier)
* Szpiedzy autowara:                                                    0/8
* Żywność autowara:                                                     0/8
* Czyszczenie terenu:                                                   0/SPEC (redukuje tor Maskarady i Skażenia)
* Ekspansja autowara:                                                   0/10

**Mausożercy**

* Budowanie kampanii antyMausowej:                                      0/8
* Propagowanie Judyty Maus jako zła młodzież:                           0/8
* Propagowanie Blakenbauerów jako chroniących Mausów:                   0/10
* Wymuszanie polityczne na odwet na Złych Magach:                       0/10
* Demoralizowanie Mausów i cichy na nich odwet:                         0/10
* Kłopoty Estrelli:                                                     3/10    (1 tier)";

        public static readonly string oneFaction4Paths3Completed20ActionsToGo_Mixed = @"**Apokalipci**:

* Pięść i Topór:     8/8
* Wypędzenie cywilów:    0/20
* Rytuał kotów:      6/6
* Głaskanie ośmiornic:   10/10";

        public static readonly string twoFactionsWith9And6Paths_OList = @"1. Bzizma:
    1. Infestacja Cywili:                                                    0/8
    1. Infestacja Żywności i Ekonomii:                                       3/6     (1 tier)
    1. Regeneracja i Adaptacja Bzizmy:                                       2/n     (1 tier)
    1. Produkcja Rojów:                                                      2/8     (1 tier)
    1. Podpięcie się do leylinów:                                            6/8     (3 tier)
    1. Szpiedzy autowara:                                                    0/8
    1. Żywność autowara:                                                     0/8
    1. Czyszczenie terenu:                                                   0/SPEC (redukuje tor Maskarady i Skażenia)
    1. Ekspansja autowara:                                                   0/10
1. Mausożercy
    1. Budowanie kampanii antyMausowej:                                      0/8
    1. Propagowanie Judyty Maus jako zła młodzież:                           0/8
    1. Propagowanie Blakenbauerów jako chroniących Mausów:                   0/10
    1. Wymuszanie polityczne na odwet na Złych Magach:                       0/10
    1. Demoralizowanie Mausów i cichy na nich odwet:                         0/10
    1. Kłopoty Estrelli:                                                     3/10    (1 tier)";

        public static readonly string oneFaction4Paths3Completed20ActionsToGo_OList = @"1. Apokalipci:
    1. Pięść i Topór:     8/8
    1. Wypędzenie cywilów:    0/20
    1. Rytuał kotów:      6/6
    1. Głaskanie ośmiornic:   10/10";

    }
}

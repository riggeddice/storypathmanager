﻿using StoryPathManager.Technical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryPathManager.Domain.StoryActions.Create
{
    public class CreateAddSingleStoryAction
    {
        public AddSingleStoryAction WithDefaultConfiguration()
        {
            return new AddSingleStoryAction(new RandomGenerator());
        }

        public AddSingleStoryAction WithCustomRandomGenerator(RandomGenerator gen)
        {
            return new AddSingleStoryAction(gen);
        }
    }
}

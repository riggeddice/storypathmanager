﻿using StoryPathManager.Technical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoryPathManager.Entities.Faction;
using StoryPathManager.Entities.Storypath;

namespace StoryPathManager.Domain.StoryActions
{
    public class AddSingleStoryAction
    {
        private RandomGenerator _randomGen;

        public AddSingleStoryAction(RandomGenerator rgen)
        {
            this._randomGen = rgen;
        }

        public List<StoryFaction> IncrementRandomStoryPathInFactions(List<StoryFaction> factions)
        {
            foreach (StoryFaction faction in factions)
            {
                List<int> incrementable = faction.IncrementableStoryPathIndexes();
                int selectedIndex = _randomGen.GenerateFromRange(incrementable);
                StoryPath selectedPath = faction.StoryPaths[selectedIndex];
                selectedPath.IncrementCurrentValueBy(1);
            }

            return factions;
        }

    }
}

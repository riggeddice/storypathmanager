﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoryPathManager.Entities.ChangeFactionPath;
using StoryPathManager.Entities.Faction;

namespace StoryPathManager.Domain.StoryActions.AddMultiple
{
    public class AddXStoryActionsToSpecificPath
    {
        public List<StoryFaction> Do(List<StoryFaction> factions, PathValueChange change)
        {
            var selectedFaction = factions[change.FactionIndex];
            var selectedPath = selectedFaction.StoryPaths[change.PathIndex];
            selectedPath.IncrementCurrentValueBy(change.Intensity);
            return factions;
        }
    }
}

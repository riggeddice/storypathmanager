﻿using StoryPathManager.Entities.ChangeFactionPath;
using StoryPathManager.Entities.Faction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryPathManager.Domain.StoryActions.AddMultiple
{
    public class RemoveXStoryActionsFromSpecificPath
    {
        public List<StoryFaction> Do(List<StoryFaction> factions, PathValueChange change)
        {
            var selectedFaction = factions[change.FactionIndex];
            var selectedPath = selectedFaction.StoryPaths[change.PathIndex];
            selectedPath.IncrementCurrentValueBy(change.Intensity * (-1));
            return factions;
        }
    }
}

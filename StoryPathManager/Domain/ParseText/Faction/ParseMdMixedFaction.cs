﻿using StoryPathManager.Technical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryPathManager.Domain.ParseText.Faction
{
    public class ParseMdMixedFaction : IParseFactionText
    {
        // A better regex is possible to be placed here. But better regex-fu tends to lead to lots of confusion.
        // This one matches the name of a Faction and the first line of the list, so I can make a reliable
        // index and then split it as needed. Low-end, barbaric, but works.
        private string _factionWithFirstPathRegex = @"\*\*.+\*\*\W+?\s\* .+\s";
        private string _factionNameOnlyRegex = @"\*\*(.+)\*\*";

        public string ExtractFactionBody(string singleFactionText)
        {
            return singleFactionText.Substring(singleFactionText.IndexOf("\n* ") - 1).Trim();
        }

        public string ExtractFactionName(string singleFactionText)
        {
            string factionNameBold = new RegexQuery().ExtractRegexedText(_factionNameOnlyRegex, singleFactionText)[0];
            string factionName = factionNameBold.Substring(2, factionNameBold.Length - 4);
            return factionName;
        }

        public List<string> SplitIntoSingleFactionTextBlocks(string multipleFactionsText)
        {
            List<string> uniqueSeparatorStringsInFaction = new RegexQuery().ExtractRegexedText(
                _factionWithFirstPathRegex, multipleFactionsText);

            List<string> singleFactionTexts = new List<string>();

            for (int i = 0; i < uniqueSeparatorStringsInFaction.Count; i++)
            {
                int start = multipleFactionsText.IndexOf(uniqueSeparatorStringsInFaction[i]);

                int end;
                if (i == uniqueSeparatorStringsInFaction.Count - 1)
                {
                    end = multipleFactionsText.Length;
                }
                else
                {
                    end = multipleFactionsText.IndexOf(uniqueSeparatorStringsInFaction[i + 1]);
                }

                string factionText = multipleFactionsText.Substring(start, end - start);
                singleFactionTexts.Add(factionText);

            }

            return singleFactionTexts;
        }

    }
}

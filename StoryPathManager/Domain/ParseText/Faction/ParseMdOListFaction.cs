﻿using StoryPathManager.Technical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryPathManager.Domain.ParseText.Faction
{
    public class ParseMdOListFaction : IParseFactionText
    {
        // A better regex is possible to be placed here. But better regex-fu tends to lead to lots of confusion.
        // This one matches the "1. text\n    1.text " and then split it as needed. Low-end, barbaric, but works.
        private string _factionWithPartOfFirstPathRegex = @"^\d+\.\s+.+";
        private string _factionNameOnlyRegex = @"^\d+\.\s+(.+)";
        private string _factionBodyOnlyRegex = @"^\s+\d+\.\s.+";

        public string ExtractFactionBody(string singleFactionText)
        {
            List<string> bodyLines = new RegexQuery().ExtractRegexedText(_factionBodyOnlyRegex, singleFactionText);
            string factionBody = String.Join("\n", bodyLines);
            return factionBody;
        }

        public string ExtractFactionName(string singleFactionText)
        {
            string factionNameBold = new RegexQuery().ExtractRegexedText(_factionNameOnlyRegex, singleFactionText)[0];
            string factionName = factionNameBold.Trim().Substring(3, factionNameBold.Length - 4);
            return factionName;
        }

        public List<string> SplitIntoSingleFactionTextBlocks(string multipleFactionsText)
        {
            List<string> uniqueSeparatorStringsInFaction = new RegexQuery().ExtractRegexedText(
                _factionWithPartOfFirstPathRegex, multipleFactionsText);

            List<string> singleFactionTexts = new List<string>();

            for (int i = 0; i < uniqueSeparatorStringsInFaction.Count; i++)
            {
                int start = multipleFactionsText.IndexOf(uniqueSeparatorStringsInFaction[i]);

                int end;
                if (i == uniqueSeparatorStringsInFaction.Count - 1)
                {
                    end = multipleFactionsText.Length;
                }
                else
                {
                    end = multipleFactionsText.IndexOf(uniqueSeparatorStringsInFaction[i + 1]);
                }

                string factionText = multipleFactionsText.Substring(start, end - start);
                singleFactionTexts.Add(factionText);

            }

            return singleFactionTexts;
        }
    }
}

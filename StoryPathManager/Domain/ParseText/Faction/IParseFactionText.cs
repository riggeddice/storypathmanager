﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryPathManager.Domain.ParseText.Faction
{
    public interface IParseFactionText
    {
        List<string> SplitIntoSingleFactionTextBlocks(string multipleFactionsText);
        string ExtractFactionName(string singleFactionText);
        string ExtractFactionBody(string singleFactionText);
    }
}

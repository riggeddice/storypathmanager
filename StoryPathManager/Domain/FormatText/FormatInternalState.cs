﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoryPathManager.Entities.Faction;
using StoryPathManager.Entities.Storypath;

namespace StoryPathManager.Domain.FormatText
{
    public class FormatInternalState
    {
        public string Factions(List<StoryFaction> factions)
        {
            string NL = Environment.NewLine;
            StringBuilder combinedText = new StringBuilder();

            int factionNumber = 0;
            foreach(StoryFaction faction in factions)
            {
                factionNumber++;
                combinedText.Append(factionNumber.ToString() +". ");
                combinedText.Append(faction.Name + NL);

                int pathNumber = 0;
                foreach(StoryPath path in faction.StoryPaths)
                {
                    pathNumber++;
                    combinedText.Append("    " + pathNumber.ToString() + ". ");
                    combinedText.Append(path.ToDisplay());
                    combinedText.Append(NL);
                }
                
            }

            return combinedText.ToString();
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoryPathManager.Entities.AddFactionPath;
using StoryPathManager.Entities.Faction;

namespace StoryPathManager.Domain.StoryFactionsPaths
{
    public class AddNewStoryPathToSpecificFaction
    {
        public List<StoryFaction> Do(List<StoryFaction> factions, PathAdditionChange change)
        {
            var selectedFaction = factions[change.FactionIndex];
            selectedFaction.AddPath(change.PathName, change.InitialValue, change.MaxValue);
            return factions;
        }
    }
}

﻿using StoryPathManager.UI.Actions;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace StoryPathManager
{
    public partial class StoryPathManager : Form
    {
        public StoryPathManager()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
        }

        // Press all the buttons

        private void button1_Click(object sender, EventArgs e)
        {
            new PressParseInput().Do(this, txtCommsWindow.Text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            new PressAddSingleAction().Do(this);
        }

        private void btnAddXActions_Click(object sender, EventArgs e)
        {
            new PressAddXActions().Do(this, txtAddXActions.Text);
        }

        private void btnRemoveXActions_Click(object sender, EventArgs e)
        {
            new PressRemoveXActions().Do(this, txtRemoveXActions.Text);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        // Change the display

        public void UpdateOutputText(string formattedOutput)
        {
            txtStatusWindow.Text = formattedOutput;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void btnAddStoryPath_Click(object sender, EventArgs e)
        {
            new PressAddPathToFaction().Do(this, txtAddStoryPath.Text);
        }
    }
}

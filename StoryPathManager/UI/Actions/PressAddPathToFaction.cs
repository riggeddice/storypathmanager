﻿using StoryPathManager.Domain.FormatText;
using StoryPathManager.Domain.StoryFactionsPaths;
using StoryPathManager.Entities.AddFactionPath;
using StoryPathManager.Entities.AddFactionPath.Create;
using StoryPathManager.Entities.Faction;
using StoryPathManager.Entities.StateContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryPathManager.UI.Actions
{
    public class PressAddPathToFaction
    {
        public void Do(StoryPathManager caller, string parameters)
        {
            List<StoryFaction> factions = SharedStateContainer.RetrieveFactions();
            PathAdditionChange change = new CreatePathAdditionChange().FromText(parameters);

            factions = new AddNewStoryPathToSpecificFaction().Do(factions, change);
            SharedStateContainer.Store(factions);

            string formattedOutput = new FormatInternalState().Factions(factions);
            caller.UpdateOutputText(formattedOutput);
        }
    }
}

﻿using StoryPathManager.Domain.FormatText;
using StoryPathManager.Domain.StoryActions;
using StoryPathManager.Domain.StoryActions.Create;
using StoryPathManager.Entities.Faction;
using StoryPathManager.Entities.StateContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryPathManager.UI.Actions
{
    public class PressAddSingleAction
    {
        public void Do(StoryPathManager caller)
        {
            List<StoryFaction> factions = SharedStateContainer.RetrieveFactions();

            AddSingleStoryAction addSingleAction = new CreateAddSingleStoryAction().WithDefaultConfiguration();
            factions = addSingleAction.IncrementRandomStoryPathInFactions(factions);
            SharedStateContainer.Store(factions);

            string formattedOutput = new FormatInternalState().Factions(factions);
            caller.UpdateOutputText(formattedOutput);
        }
    }

}

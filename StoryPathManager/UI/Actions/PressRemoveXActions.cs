﻿using StoryPathManager.Domain.FormatText;
using StoryPathManager.Domain.StoryActions.AddMultiple;
using StoryPathManager.Entities.ChangeFactionPath;
using StoryPathManager.Entities.ChangeFactionPath.Create;
using StoryPathManager.Entities.Faction;
using StoryPathManager.Entities.StateContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryPathManager.UI.Actions
{
    public class PressRemoveXActions
    {
        public void Do(StoryPathManager caller, string parameters)
        {
            List<StoryFaction> factions = SharedStateContainer.RetrieveFactions();
            PathValueChange change = new CreatePathValueChange().FromText(parameters);

            factions = new RemoveXStoryActionsFromSpecificPath().Do(factions, change);
            SharedStateContainer.Store(factions);

            string formattedOutput = new FormatInternalState().Factions(factions);
            caller.UpdateOutputText(formattedOutput);
        }
    }
}

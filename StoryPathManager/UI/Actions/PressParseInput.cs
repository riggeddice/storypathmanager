﻿using StoryPathManager.Domain.FormatText;
using StoryPathManager.Entities.Faction;
using StoryPathManager.Entities.Faction.Create;
using StoryPathManager.Entities.StateContainer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryPathManager.UI.Actions
{
    public class PressParseInput
    {
        public void Do(StoryPathManager caller, string inputText)
        {
            List<StoryFaction> factions = new CreateFaction().MultipleFactions(inputText);
            SharedStateContainer.Store(factions);

            string formattedOutput = new FormatInternalState().Factions(factions);
            caller.UpdateOutputText(formattedOutput);
        }
    }
}

﻿namespace StoryPathManager
{
    partial class StoryPathManager
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnParseInput = new System.Windows.Forms.Button();
            this.btnAddOneAction = new System.Windows.Forms.Button();
            this.txtCommsWindow = new System.Windows.Forms.TextBox();
            this.txtStatusWindow = new System.Windows.Forms.TextBox();
            this.btnExit = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txtRemoveXActions = new System.Windows.Forms.TextBox();
            this.txtAddXActions = new System.Windows.Forms.TextBox();
            this.btnRemoveXActions = new System.Windows.Forms.Button();
            this.btnAddXActions = new System.Windows.Forms.Button();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.txtUnfreezeStoryPath = new System.Windows.Forms.TextBox();
            this.btnUnfreezeStoryPath = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFreezeStoryPath = new System.Windows.Forms.TextBox();
            this.txtAddStoryPath = new System.Windows.Forms.TextBox();
            this.btnFreezeStoryPath = new System.Windows.Forms.Button();
            this.btnAddStoryPath = new System.Windows.Forms.Button();
            this.txtAppCommWindow = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnParseInput
            // 
            this.btnParseInput.Location = new System.Drawing.Point(12, 12);
            this.btnParseInput.Name = "btnParseInput";
            this.btnParseInput.Size = new System.Drawing.Size(93, 23);
            this.btnParseInput.TabIndex = 0;
            this.btnParseInput.Text = "Parse Input";
            this.btnParseInput.UseVisualStyleBackColor = true;
            this.btnParseInput.Click += new System.EventHandler(this.button1_Click);
            // 
            // btnAddOneAction
            // 
            this.btnAddOneAction.Location = new System.Drawing.Point(6, 19);
            this.btnAddOneAction.Name = "btnAddOneAction";
            this.btnAddOneAction.Size = new System.Drawing.Size(172, 23);
            this.btnAddOneAction.TabIndex = 2;
            this.btnAddOneAction.Text = "Add One Action To All Factions";
            this.btnAddOneAction.UseVisualStyleBackColor = true;
            this.btnAddOneAction.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtCommsWindow
            // 
            this.txtCommsWindow.Location = new System.Drawing.Point(12, 41);
            this.txtCommsWindow.Multiline = true;
            this.txtCommsWindow.Name = "txtCommsWindow";
            this.txtCommsWindow.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtCommsWindow.Size = new System.Drawing.Size(299, 422);
            this.txtCommsWindow.TabIndex = 5;
            // 
            // txtStatusWindow
            // 
            this.txtStatusWindow.Location = new System.Drawing.Point(329, 41);
            this.txtStatusWindow.Multiline = true;
            this.txtStatusWindow.Name = "txtStatusWindow";
            this.txtStatusWindow.ScrollBars = System.Windows.Forms.ScrollBars.Both;
            this.txtStatusWindow.Size = new System.Drawing.Size(572, 489);
            this.txtStatusWindow.TabIndex = 6;
            // 
            // btnExit
            // 
            this.btnExit.Location = new System.Drawing.Point(819, 651);
            this.btnExit.Name = "btnExit";
            this.btnExit.Size = new System.Drawing.Size(82, 23);
            this.btnExit.TabIndex = 7;
            this.btnExit.Text = "Quit";
            this.btnExit.UseVisualStyleBackColor = true;
            this.btnExit.Click += new System.EventHandler(this.button5_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtRemoveXActions);
            this.groupBox1.Controls.Add(this.txtAddXActions);
            this.groupBox1.Controls.Add(this.btnRemoveXActions);
            this.groupBox1.Controls.Add(this.btnAddOneAction);
            this.groupBox1.Controls.Add(this.btnAddXActions);
            this.groupBox1.Location = new System.Drawing.Point(12, 541);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(385, 133);
            this.groupBox1.TabIndex = 13;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "StoryActions";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(181, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(186, 13);
            this.label1.TabIndex = 17;
            this.label1.Text = "FactionNumber, PathNumber, Amount";
            this.label1.Click += new System.EventHandler(this.label1_Click);
            // 
            // txtRemoveXActions
            // 
            this.txtRemoveXActions.Location = new System.Drawing.Point(184, 102);
            this.txtRemoveXActions.Name = "txtRemoveXActions";
            this.txtRemoveXActions.Size = new System.Drawing.Size(189, 20);
            this.txtRemoveXActions.TabIndex = 16;
            // 
            // txtAddXActions
            // 
            this.txtAddXActions.Location = new System.Drawing.Point(184, 65);
            this.txtAddXActions.Name = "txtAddXActions";
            this.txtAddXActions.Size = new System.Drawing.Size(189, 20);
            this.txtAddXActions.TabIndex = 15;
            // 
            // btnRemoveXActions
            // 
            this.btnRemoveXActions.Location = new System.Drawing.Point(6, 102);
            this.btnRemoveXActions.Name = "btnRemoveXActions";
            this.btnRemoveXActions.Size = new System.Drawing.Size(172, 23);
            this.btnRemoveXActions.TabIndex = 14;
            this.btnRemoveXActions.Text = "Remove X Actions From Path";
            this.btnRemoveXActions.UseVisualStyleBackColor = true;
            this.btnRemoveXActions.Click += new System.EventHandler(this.btnRemoveXActions_Click);
            // 
            // btnAddXActions
            // 
            this.btnAddXActions.Location = new System.Drawing.Point(6, 63);
            this.btnAddXActions.Name = "btnAddXActions";
            this.btnAddXActions.Size = new System.Drawing.Size(172, 23);
            this.btnAddXActions.TabIndex = 13;
            this.btnAddXActions.Text = "Add X Actions To Path";
            this.btnAddXActions.UseVisualStyleBackColor = true;
            this.btnAddXActions.Click += new System.EventHandler(this.btnAddXActions_Click);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.txtUnfreezeStoryPath);
            this.groupBox2.Controls.Add(this.btnUnfreezeStoryPath);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtFreezeStoryPath);
            this.groupBox2.Controls.Add(this.txtAddStoryPath);
            this.groupBox2.Controls.Add(this.btnFreezeStoryPath);
            this.groupBox2.Controls.Add(this.btnAddStoryPath);
            this.groupBox2.Location = new System.Drawing.Point(412, 536);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(401, 133);
            this.groupBox2.TabIndex = 14;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "StoryPaths";
            // 
            // txtUnfreezeStoryPath
            // 
            this.txtUnfreezeStoryPath.Location = new System.Drawing.Point(184, 105);
            this.txtUnfreezeStoryPath.Name = "txtUnfreezeStoryPath";
            this.txtUnfreezeStoryPath.Size = new System.Drawing.Size(189, 20);
            this.txtUnfreezeStoryPath.TabIndex = 20;
            // 
            // btnUnfreezeStoryPath
            // 
            this.btnUnfreezeStoryPath.Location = new System.Drawing.Point(6, 105);
            this.btnUnfreezeStoryPath.Name = "btnUnfreezeStoryPath";
            this.btnUnfreezeStoryPath.Size = new System.Drawing.Size(172, 23);
            this.btnUnfreezeStoryPath.TabIndex = 19;
            this.btnUnfreezeStoryPath.Text = "Unfreeze StoryPath in Faction";
            this.btnUnfreezeStoryPath.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(181, 62);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(144, 13);
            this.label3.TabIndex = 18;
            this.label3.Text = "FactionNumber, PathNumber";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(181, 10);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(190, 13);
            this.label2.TabIndex = 17;
            this.label2.Text = "FctNo, InitVal, MaxVal, NewPathName";
            // 
            // txtFreezeStoryPath
            // 
            this.txtFreezeStoryPath.Location = new System.Drawing.Point(184, 78);
            this.txtFreezeStoryPath.Name = "txtFreezeStoryPath";
            this.txtFreezeStoryPath.Size = new System.Drawing.Size(189, 20);
            this.txtFreezeStoryPath.TabIndex = 16;
            // 
            // txtAddStoryPath
            // 
            this.txtAddStoryPath.Location = new System.Drawing.Point(184, 26);
            this.txtAddStoryPath.Name = "txtAddStoryPath";
            this.txtAddStoryPath.Size = new System.Drawing.Size(189, 20);
            this.txtAddStoryPath.TabIndex = 15;
            // 
            // btnFreezeStoryPath
            // 
            this.btnFreezeStoryPath.Location = new System.Drawing.Point(6, 76);
            this.btnFreezeStoryPath.Name = "btnFreezeStoryPath";
            this.btnFreezeStoryPath.Size = new System.Drawing.Size(172, 23);
            this.btnFreezeStoryPath.TabIndex = 14;
            this.btnFreezeStoryPath.Text = "Freeze StoryPath in Faction";
            this.btnFreezeStoryPath.UseVisualStyleBackColor = true;
            // 
            // btnAddStoryPath
            // 
            this.btnAddStoryPath.Location = new System.Drawing.Point(6, 24);
            this.btnAddStoryPath.Name = "btnAddStoryPath";
            this.btnAddStoryPath.Size = new System.Drawing.Size(172, 23);
            this.btnAddStoryPath.TabIndex = 13;
            this.btnAddStoryPath.Text = "Add New StoryPath to Faction";
            this.btnAddStoryPath.UseVisualStyleBackColor = true;
            this.btnAddStoryPath.Click += new System.EventHandler(this.btnAddStoryPath_Click);
            // 
            // txtAppCommWindow
            // 
            this.txtAppCommWindow.Location = new System.Drawing.Point(12, 482);
            this.txtAppCommWindow.Multiline = true;
            this.txtAppCommWindow.Name = "txtAppCommWindow";
            this.txtAppCommWindow.ScrollBars = System.Windows.Forms.ScrollBars.Horizontal;
            this.txtAppCommWindow.Size = new System.Drawing.Size(299, 48);
            this.txtAppCommWindow.TabIndex = 15;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 466);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(175, 13);
            this.label4.TabIndex = 18;
            this.label4.Text = "Application communication window:";
            // 
            // StoryPathManager
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(913, 686);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtAppCommWindow);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnExit);
            this.Controls.Add(this.txtStatusWindow);
            this.Controls.Add(this.txtCommsWindow);
            this.Controls.Add(this.btnParseInput);
            this.Name = "StoryPathManager";
            this.Text = "StoryPathManager";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnParseInput;
        private System.Windows.Forms.Button btnAddOneAction;
        private System.Windows.Forms.TextBox txtCommsWindow;
        private System.Windows.Forms.TextBox txtStatusWindow;
        private System.Windows.Forms.Button btnExit;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtRemoveXActions;
        private System.Windows.Forms.TextBox txtAddXActions;
        private System.Windows.Forms.Button btnRemoveXActions;
        private System.Windows.Forms.Button btnAddXActions;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TextBox txtUnfreezeStoryPath;
        private System.Windows.Forms.Button btnUnfreezeStoryPath;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtFreezeStoryPath;
        private System.Windows.Forms.TextBox txtAddStoryPath;
        private System.Windows.Forms.Button btnFreezeStoryPath;
        private System.Windows.Forms.Button btnAddStoryPath;
        private System.Windows.Forms.TextBox txtAppCommWindow;
        private System.Windows.Forms.Label label4;
    }
}


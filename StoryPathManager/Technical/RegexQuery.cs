﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StoryPathManager.Technical
{
    public class RegexQuery
    {
        public List<string> ExtractRegexedText(string regexQuery, string textBlockToParse)
        {
            Regex regexObj = new Regex(regexQuery, RegexOptions.Multiline);
            Match matchResults = regexObj.Match(textBlockToParse);

            List<string> results = new List<string>();
            while (matchResults.Success)
            {
                results.Add(matchResults.Value);
                matchResults = matchResults.NextMatch();
            }

            return results;
        }

    }
}

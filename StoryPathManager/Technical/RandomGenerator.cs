﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryPathManager.Technical
{
    public class RandomGenerator
    {
        private Random _random;

        public RandomGenerator()
        {
            _random = new Random();
        }

        public virtual int GenerateBetween(int minRange, int maxRange)
        {
            return _random.Next(minRange, maxRange);
        }

        public virtual int GenerateFromRange(List<int> rangeOfNumbers)
        {
            int spread = rangeOfNumbers.Count;
            int index = this.GenerateBetween(0, spread - 1);
            return rangeOfNumbers[index];
        }
    }

}

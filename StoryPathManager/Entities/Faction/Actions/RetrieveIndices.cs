﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoryPathManager.Entities.Storypath;

namespace StoryPathManager.Entities.Faction.Actions
{
    public class RetrieveIndices
    {
        public List<int> OnlyIncrementable(List<StoryPath> storyPaths)
        {
            List<int> incrementable = new List<int>();

            for (int i=0; i<storyPaths.Count; i++)
            {
                StoryPath considered = storyPaths[i];
                if(considered.CurrentValue < considered.MaxValue)
                {
                    incrementable.Add(i);
                }
            }

            return incrementable;
        }
    }
}

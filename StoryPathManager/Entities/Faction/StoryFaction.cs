﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoryPathManager.Entities.Storypath;
using StoryPathManager.Entities.Faction.Actions;
using StoryPathManager.Entities.Storypath.Create;

namespace StoryPathManager.Entities.Faction
{
    public class StoryFaction
    {
        private string _factionName;
        private List<StoryPath> _storyPaths;

        public StoryFaction(string factionName, List<StoryPath> storyPaths)
        {
            this._factionName = factionName;
            this._storyPaths = storyPaths;
        }

        public void AddPath(string pathName, int startingLevel, int maxLevel)
        {
            StoryPath newPath = new CreateStoryPath().SingleStoryPath(pathName, startingLevel, maxLevel);
            _storyPaths.Add(newPath);
        }

        public string Name => this._factionName;
        public List<StoryPath> StoryPaths => this._storyPaths;

        public List<int> IncrementableStoryPathIndexes()
        {
            return new RetrieveIndices().OnlyIncrementable(_storyPaths);
        }
    }
}

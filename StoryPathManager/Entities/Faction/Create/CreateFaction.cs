﻿using StoryPathManager.Entities.Storypath.Create;
using StoryPathManager.Entities.Storypath;
using StoryPathManager.Technical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using StoryPathManager.Domain.ParseText.Faction;

namespace StoryPathManager.Entities.Faction.Create
{
    public class CreateFaction
    {
        private IParseFactionText _parser;

        public CreateFaction()
        {
            _parser = new ParseMdOListFaction();
        }

        public List<StoryFaction> MultipleFactions(string multipleFactionsText)
        {
            List<string> singleFactionTexts = _parser.SplitIntoSingleFactionTextBlocks(multipleFactionsText);

            List<StoryFaction> factions = new List<StoryFaction>();
            foreach (string text in singleFactionTexts)
            {
                StoryFaction singleFaction = SingleFaction(text);
                factions.Add(singleFaction);
            }

            return factions;
        }

        public StoryFaction SingleFaction(string singleFactionText)
        {
            string factionName = _parser.ExtractFactionName(singleFactionText).Trim();
            string factionBody = _parser.ExtractFactionBody(singleFactionText);

            List<StoryPath> storyPaths = new CreateStoryPath().MultipleStoryPaths(factionBody);

            StoryFaction faction = new StoryFaction(factionName, storyPaths);
            return faction;
        }

    }
    
}

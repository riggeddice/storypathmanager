﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryPathManager.Entities.Storypath.Actions
{
    public class IncrementStoryPathCurrentValue
    {
        public int WithConstraints(int currentVal, int maxVal, int incrementValue)
        {
            if(currentVal + incrementValue > maxVal)
            {
                return maxVal;
            }
            if(currentVal + incrementValue < 0)
            {
                return 0;
            }
            else
            {
                return currentVal + incrementValue;
            }
        }
    }
}

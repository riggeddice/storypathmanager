﻿using StoryPathManager.Technical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StoryPathManager.Entities.Storypath.Create
{
    public class CreateStoryPath
    {
        //"    1. Name Of Faction 1:    6/14  \n"
        //"    1. Name Of Faction 2:    0/SPEC"
        private string _singleStoryPath = @"^    \d+\. (.+):\s+(\d+)\/(\S+)\s*?.*$";

        public List<StoryPath> MultipleStoryPaths(string factionBody)
        {
            Regex regexObj = new Regex(_singleStoryPath, RegexOptions.Multiline);
            MatchCollection matches = regexObj.Matches(factionBody);

            List<StoryPath> storypaths = new List<StoryPath>();
            foreach (Match match in matches)
            {
                var groups = match.Groups;

                string storyName = groups[1].ToString();
                string currentLevel = groups[2].ToString();
                string maxLevel = groups[3].ToString();

                StoryPath path = this.SingleStoryPath(storyName, currentLevel, maxLevel);
                storypaths.Add(path);

            }

            return storypaths;
        }

        public StoryPath SingleStoryPath(string storyName, int currentLevel, int maxLevel)
        {
            return new StoryPath(storyName, currentLevel, maxLevel);
        }

        public StoryPath SingleStoryPath(string storyName, string currentLevel, string maxLevel)
        {
            if(int.TryParse(currentLevel, out int currentVal) == false)
            {
                throw new ArgumentException("Current level at " + storyName + " is not a number");
            }

            if (int.TryParse(maxLevel, out int maxVal) == false)
            {
                maxVal = int.MaxValue;
            }

            return this.SingleStoryPath(storyName, currentVal, maxVal);
        }
    }
}

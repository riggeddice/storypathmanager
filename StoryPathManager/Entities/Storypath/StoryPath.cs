﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoryPathManager.Entities.ChangeFactionPath;
using StoryPathManager.Entities.Storypath.Actions;

namespace StoryPathManager.Entities.Storypath
{
    public class StoryPath
    {
        private string _pathName;
        private int _currentVal;
        private int _maxVal;


        public StoryPath(string storyName, int currentVal, int maxVal)
        {
            this._pathName = storyName;
            this._currentVal = currentVal;
            this._maxVal = maxVal;
        }


        public int CurrentValue => this._currentVal;
        public int MaxValue => this._maxVal;
        public string PathName => this._pathName;


        public string ToDisplay()
        {
            return this._pathName + ": " + this._currentVal + "/" + this._maxVal;
        }

        public void IncrementCurrentValueBy(int incrementValue)
        {
            this._currentVal = new IncrementStoryPathCurrentValue().WithConstraints(this._currentVal, this._maxVal, incrementValue);
        }
    }
}

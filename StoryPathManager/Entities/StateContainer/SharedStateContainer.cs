﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using StoryPathManager.Entities.Faction;
using StoryPathManager.Technical;
using StoryPathManager.Domain.FormatText;

namespace StoryPathManager.Entities.StateContainer
{
    public static class SharedStateContainer
    { 

        private static StoreToHdd _store = new StoreToHdd();
        private static List<StoryFaction> _factions = new List<StoryFaction>();

        public static void Store(List<StoryFaction> factions)
        {
            _factions = factions;

            string storedFactions = new FormatInternalState().Factions(factions);
            _store.StringInDefaultLocation(storedFactions);
        }

        public static List<StoryFaction> RetrieveFactions()
        {
            if (_factions == null)
            {
                return new List<StoryFaction>();
            }
            else
            {
                return _factions;
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryPathManager.Entities.ChangeFactionPath
{
    public class PathValueChange
    {
        private int _factionIndex;
        private int _pathIndex;
        private int _intensity;

        public PathValueChange(int factionIndex, int pathIndex, int intensity)
        {
            this._factionIndex = factionIndex;
            this._pathIndex = pathIndex;
            this._intensity = intensity;
        }

        // Index recalibration from 1-based to 0-based
        public int FactionIndex => _factionIndex - 1;

        // Index recalibration from 1-based to 0-based
        public int PathIndex => _pathIndex - 1;

        // Intensity is NOT an index, no recalibration needed
        public int Intensity => _intensity;

        public Tuple<int, int, int> AllFields()
        {
            return new Tuple<int, int, int>(_factionIndex, _pathIndex, _intensity);
        }
    }
}

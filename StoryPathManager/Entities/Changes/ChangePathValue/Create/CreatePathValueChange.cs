﻿using StoryPathManager.Technical;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StoryPathManager.Entities.ChangeFactionPath.Create
{
    public class CreatePathValueChange
    {
        public PathValueChange FromText(string parameters)
        {
            string threeGroups = @"\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*";

            Regex regexObj = new Regex(threeGroups, RegexOptions.Multiline);
            Match matchResults = regexObj.Match(parameters);

            if(matchResults.Groups.Count == 4)
            {
                int factionIndex = int.Parse(matchResults.Groups[1].Value);
                int pathIndex = int.Parse(matchResults.Groups[2].Value);
                int intensity = int.Parse(matchResults.Groups[3].Value);

                return new PathValueChange(factionIndex, pathIndex, intensity);
            }
            else
            {
                return this.NoChange();
            }
        }

        public PathValueChange NoChange()
        {
            return new PathValueChange(0, 0, 0);
        }

    }
}

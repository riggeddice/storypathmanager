﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace StoryPathManager.Entities.AddFactionPath
{
    public class PathAdditionChange
    {
        private int _indexOfFactionToAddPathTo;
        private string _nameOfNewPath;
        private int _initialValueOfNewPath;
        private int _maxValueOfNewPath;

        public PathAdditionChange(int factionIndex, int initialValue, int maxValue, string pathName)
        {
            this._indexOfFactionToAddPathTo = factionIndex;
            this._nameOfNewPath = pathName;
            this._initialValueOfNewPath = initialValue;
            this._maxValueOfNewPath = maxValue;
        }

        public int FactionIndex => this._indexOfFactionToAddPathTo - 1;
        public string PathName => this._nameOfNewPath;
        public int InitialValue => this._initialValueOfNewPath;
        public int MaxValue => this._maxValueOfNewPath;

        public Tuple<int, string, int, int> AllFields()
        {
            return new Tuple<int, string, int, int>(_indexOfFactionToAddPathTo, _nameOfNewPath, _initialValueOfNewPath, _maxValueOfNewPath);
        }
    }
}

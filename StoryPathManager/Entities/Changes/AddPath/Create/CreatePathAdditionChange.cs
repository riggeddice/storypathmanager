﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace StoryPathManager.Entities.AddFactionPath.Create
{
    public class CreatePathAdditionChange
    {
        public PathAdditionChange FromText(string parameters)
        {
            // May have whitespaces. Will start from faction number and ','. Then apply the same to init and max values.
            // Afterwards, everything past the comma and extra whitespaces is a faction name - thus .+ group.
            string twoGroups = @"\s*(\d+)\s*,\s*(\d+)\s*,\s*(\d+)\s*,\s*(.+)";

            Regex regexObj = new Regex(twoGroups, RegexOptions.Multiline);
            Match matchResults = regexObj.Match(parameters);

            if (matchResults.Groups.Count == 5)
            {
                int factionIndex = int.Parse(matchResults.Groups[1].Value);
                int initValue = int.Parse(matchResults.Groups[2].Value);
                int maxValue = int.Parse(matchResults.Groups[3].Value);
                string newPathNameCandidate = matchResults.Groups[4].Value;
                string newPathName = newPathNameCandidate.Trim();

                return new PathAdditionChange(factionIndex, initValue, maxValue, newPathName);
            }
            else
            {
                return this.NoChange();
            }
        }

        public PathAdditionChange NoChange()
        {
            return new PathAdditionChange(0, 0, 0, string.Empty);
        }
    }
}
